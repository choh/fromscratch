# Fromscratch

This is a package in which I implement methods and algorithms from the world
of statistical learning myself, ideally in both R and C++.
For everything in here there are most certainly better implementations 
available.
Creating better implementations compared to what is already available is 
however not the point here, this is mainly a playground for learning.
While I think the implementations here are generally sound, you should probably
use more established tools for productive use. 