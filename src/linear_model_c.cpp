#include <RcppArmadillo.h>
using namespace Rcpp;
using namespace arma;

// [[Rcpp::export]]
List linear_model_c(DataFrame xcols, NumericVector ycol, bool intercept) {
    int x_rows = xcols.nrows();
    int x_cols = xcols.size();
    NumericMatrix xm(x_rows, x_cols);

    for (int i = 0; i < x_cols; i++) {
        xm(_, i) = NumericVector(xcols[i]);
    }

    vec yvec = as<vec>(ycol);
    mat xmat = as<mat>(xm);

    if (intercept) {
        xmat.insert_cols(x_cols, ones<vec>(x_rows));
    }

    mat xtx_inv = inv(xmat.t() * xmat);
    vec coefs = xtx_inv * xmat.t() * yvec;

    vec fitted = xmat * coefs;
    vec residuals = yvec - fitted;
    vec res_sq = pow(residuals, 2);

    // x_cols does not include the intercept, thus we substract by 2
    double df = x_rows - x_cols - 1;
    double variance_est = (1.0 / df) * sum(res_sq);
    mat var_cov_mat = xtx_inv * variance_est;

    vec vcv_diag = var_cov_mat.diag();
    vec stderr = sqrt(vcv_diag);

    vec tval = coefs / (sqrt(variance_est) * sqrt(xtx_inv.diag()));
    int tval_len = tval.n_elem;
    vec pval(tval_len);

    for (int i = 0; i < tval_len; i++) {
        pval[i] = 2.0 * Rf_pt(abs(tval[i]), df, false, false);
    }

    xcols[".fitted"] = fitted;
    xcols[".residuals"] = residuals;

    StringVector xnames(x_cols + 1);
    StringVector xcol_names = xcols.names();
    for (int i = 0; i <= x_cols; i++) {
        if (i < x_cols) {
            xnames[i] = xcol_names[i];
        } else {
            xnames[i] = "Intercept";
        }
    }

    DataFrame agmt_df(xcols);
    DataFrame coef_df = DataFrame::create(
        Named("predictor") = xnames,
        Named("estimate") = coefs,
        Named("error") = stderr,
        Named("t") = tval,
        Named("p") = pval
    );

    return List::create(
        Named("coefficients") = coef_df,
        Named("augmented") = agmt_df,
        Named("vcv") = var_cov_mat
    );
}

