#include <RcppArmadillo.h>
#include <math.h>
using namespace Rcpp;
using namespace arma;

//' CPP impelemtation of euclidean distance
//'
//' @param data The data to calculate the distance for.
//'
//' If the input data has rownames these are copied into the resulting
//' distance matrix row and column names.
//'
//' @return A matrix of eulidean distances in a matrix.
// [[Rcpp::export]]
NumericMatrix euclidean_distance_cpp(DataFrame data) {
    int data_rows = data.nrows();
    int data_cols = data.size();
    NumericMatrix distances(data_rows, data_rows);

    for (int i = 0; i < data_rows; i++) {
        for (int j = 0; j < data_rows; j++) {
            vec irow(data_cols);
            vec jrow(data_cols);

            for (int ii = 0; ii < data_cols; ii++) {
                NumericVector icol = data[ii];
                irow[ii] = icol[i];
            }

            for (int jj = 0; jj < data_cols; jj++) {
                NumericVector jcol = data[jj];
                jrow[jj] = jcol[j];
            }

            vec sqdiff = pow(irow - jrow, 2);
            double res = sqrt(accu(sqdiff));
            distances(i, j) = res;
            distances(j, i) = res;
        }
    }

    return distances;
}