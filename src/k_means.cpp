#include <RcppArmadillo.h>
#include "euclidean_distance.h"
using namespace Rcpp;
using namespace arma;

//' CPP implementation of k-means clustering.
//'
//' @param data The data to cluster.
//' @param k The amount of clusters to produce.
//' @param max_iterations The maximum amount of iterations for clustering
//'
//' @return A list with labels, center means and iterations taken.
// [[Rcpp::export]]
List k_means_cpp(DataFrame data, int k, int max_iterations) {
    NumericMatrix dist_matrix = euclidean_distance_cpp(data);
    int data_rows = data.nrows();
    int data_cols = data.size();

    NumericMatrix dfmatr(data_rows, data_cols);
    for (uword i = 0; i < data_cols; i++) {
        dfmatr(_, i) = NumericVector(data[i]);
    }
    mat dfmat(as<arma::mat>(dfmatr));

    vec centeridx(k);
    vec row_idxs(data_rows);
    std::iota(row_idxs.begin(), row_idxs.end(), 0);

    std::random_device rd;
    std::mt19937 g(rd());
    std::shuffle(row_idxs.begin(), row_idxs.end(), g);

    vec initial_selection(k);
    for (uword i = 0; i < k; i++) {
        initial_selection(i) = row_idxs(i);
    }

    mat center_mean(k, data_cols);
    for (uword i = 0; i < k; i++) {
        center_mean.row(i) = dfmat.row(initial_selection(i));
    }

    mat new_center_mean(k, data_cols);

    bool not_converged = true;
    int iteration_count = 0;
    mat distances;
    vec clusters(data_rows);


    while (not_converged) {
        mat all_data = join_cols(center_mean, dfmat);
        distances = as<arma::mat>(euclidean_distance_cpp(all_data));

        mat k_dist(data_rows, k);
        for (uword i = 0; i < data_rows; i++) {
            k_dist.row(i) = distances.row(i + k).head(k);
        }

        for (uword i = 0; i < data_rows; i++) {
            clusters(i) = index_min(k_dist.row(i));
        }

        for (uword i = 0; i < k; i ++) {
            uvec this_idx(clusters.size());
            int added = 0;
            for (uword j = 0; j < clusters.size(); j++) {
                int thisclu = clusters(j);
                if (thisclu == i) {
                    added++;
                    this_idx(added) = j;
                }
            }
            this_idx.resize(added);
            mat clusterselection(this_idx.size(), data_cols);
            for (uword j = 0; j < this_idx.size(); j++) {
                clusterselection.row(j) = dfmat.row(this_idx(j));
            }
            new_center_mean.row(i) = mean(clusterselection, 0);
        }

        bool is_changed = approx_equal(
            new_center_mean, center_mean, "absdiff", 0.001);

        if (is_changed) {;
            not_converged = false;
        }

        center_mean = new_center_mean;

        iteration_count++;
        if (iteration_count >= max_iterations) {
            break;
        }
    }

    NumericVector rclusters = wrap(clusters);
    rclusters.attr("dim") = R_NilValue;


    return List::create(
        Named("labels") = rclusters,
        Named("centers") = center_mean,
        Named("iterations") = iteration_count
    );
}